# from multiprocessing import context
# from re import template
from django.http import HttpResponse
from django.template import loader

def testing(request):
    template = loader.get_template('template.html')
    context = {
        'firstname':'Kizzah',
    }
    return HttpResponse(template.render(context, request))
